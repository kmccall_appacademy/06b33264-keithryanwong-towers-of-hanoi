# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def move(from_tower, to_tower)
    disc = @towers[from_tower].pop
    @towers[to_tower].push(disc)
  end

  def valid_move?(from_tower, to_tower)
    return false if from_tower > 2 || to_tower > 2

    return false if @towers[from_tower].empty?
    return true if @towers[to_tower].empty?
    @towers[to_tower].last > @towers[from_tower].last
  end

  def won?
    @towers[1].length == 3 || @towers[2].length == 3
  end

  def play
    until self.won?
      self.render
      puts 'Move from:'
      from = gets.chomp.to_i
      puts "from: #{from} to:"
      to = gets.chomp.to_i
      puts "from: #{from} to: #{to}"

      raise "invalid move" unless valid_move?(from, to)

      move(from, to)
    end

    self.render
    puts 'CONGRATS, YOU WON!!!!'
  rescue
    puts "invalid move \n"
    sleep(2)
    retry
  end

  def render
    system('clear')
    puts "Welcome to ToH\n\n"
    render_str = ''
    i = 0

    while i < 3
      row = ''
      @towers.each do |tower|
        if tower[i].nil?
          row += ' '
        else
          row += tower[i].to_s
        end
        row += ' '
      end
      render_str = row + "\n" + render_str
      i += 1
    end
    render_str += "- - -\n0 1 2"
    puts render_str
  end
end

if __FILE__ == $PROGRAM_NAME
  TowersOfHanoi.new.play
end
